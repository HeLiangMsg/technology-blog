---
title: "超轻量级 C 语言网络库：dyad"
date: 2022-05-26T23:09:59+08:00
draft: false
tags: []
categories: ["源码"]
featured_image: 
description:  超轻量级 C 语言网络库：dyad
---

# Dyad API

## Function

#### void dyad_init(void)

初始化dyad。需要在调用其它任意 dyad 函数之前调用它。

#### void dyad_update(void)

处理所有活跃stream和events。这应该在程序主循环的每次迭代中调用。 默认情况下`dyad_update()`  最多会阻塞1秒钟，`dyad_update()`可以通过设置为非阻塞，通过 `dyad_setUpdateTimeout()` 设置超时时间为0.

#### void dyad_shutdown(void)

在所有当前streams 上发送 `DYAD_EVENT_CLOSE`事件，关闭并清除所有内容。它一般在程序结束，或者当我们不再需要 dyad。

#### const char *dyad_getVersion(void)

通过字符串返回库里面的版本号。

#### double dyad_getTime(void)

返回当前的时间秒数。这个时间仅用来比较，没有特定的纪元*(epoch)*可以保证。

#### int dyad_getStreamCount(void)

返回当前活跃流的个数

#### void dyad_setUpdateTimeout(double seconds)

设置 `dyad_update()` 函数 可以阻塞的最大时间。如果 `seconds`为0，则 `dyad_update()` 不阻塞。

#### void dyad_setTickInterval(double senconds)

设置向所有流，发出`DYAD_EVENT_TICK` 事件的间隔秒数。默认值为1秒。

#### dyad_PanicCallback dyad_atPanic(dyad_PanicCallback func)

设置这个函数，当 dyad 遇到一个错误，而且还不能回复时调用。

设置当 dyad 遇到无法回复的错误时，将调用的函数。返回旧的 atPanic 函数。如果未设置，则返回`NULL`。

#### dyad_Stream * dyad_newStream(void)

创建并返回一个新的stream。返回的流默认状态为close。需要调用 `dyad_list()` ,`dyad_listenEx()`, `dyad_connect()`  来脱离 close 状态。另见 [dyad_Stream](#dyad_Stream)

#### int dyad_listen(dyad_Stream *stream, int port)

使`流`开始监听所有本地网卡的给定`端口`。

#### int dyad_listenEx(dyad_Stream *stream, const char *host, int port, int backlog)

跟 `dyad_listen()` 执行相同的任务,但提供额外选项：`host` 是 流需要监听本地网卡的地址。 `port` 是对应的端口。`backlog` 是等待连接的队列的最大长度。

#### int dyad_connect(dyad_Stream *stream, const char *host, int port)

将`stream` 连接到远程 `主机`。

#### void dyad_addListener(dyad_Stream *stream, int event, dyad_Callback callback, void *udata)

添加 `event` 的监听器到`stream`。当事件发生时，`callback` 会被调用并且event的 udate 字段 设置到 `udate`。如果为了同一个事件，添加多个监听者。则按照添加的顺序依次通知监听者。

#### void dyad_removeListerner(dyad_Stream * stream, int event, dyad_Callback callback, void *udata)

删除使用`dyad_addListener()`函数添加的事件监听器。另见 [Event](#Event)

#### void dyad_removeAllListeners(dyad_Stream * stream , int event)

删除所有给定事件的监听者。如果 `event` 为 `DYAD_EVENT_NULL` 则删除所有事件的所有监听者。

#### void dyad_write(dyad_Stream *stream, const void *data, int size)

写入给定 `size` 的 `data` 到 `stream`中。如果你想写入一个空截断字符串，使用`dyad_writef()` 代替。

#### void dyad_write(dyad_Stream *stream, const char *fmt,...)

写入一个格式化字符串到 `stream`。这个函数类似于 `sprintf()` 但，有以下不同：

- dyad负责为结果分配足够的内存。
- 没有标记、宽度和精度；仅支持以下标准说明符：`%% %s %f %g %d %i %c %x %X %p`
- `%r `说明符是支持的，它处理 `FILE* `参数并写入内容，知道遇到 EOF
- `%b` 说明符是支持的，这将采用 `void*` 参数，后跟一个 `int` 参数，表示写入的字节数量。
- `%b` 说明符是支持的，采用一个 `void*` 参数，后跟随一个int 参数表示写入的字节数量。

#### void dyad_vwritef(dyad_Stream，const char *fmt, va_list args)

`dyad_vwritef()` 和 `dyad_writef()` ，雷同于 `vsprintf()` 和`sprintf()`的关系。

#### void dyad_end(dyad_Stream *stream)

完成发送任意数据到`stream`中，遗留在写缓冲中。然后关闭这个`stream`。

完成发送遗留在写缓冲区中的任意数据到`stream`中，然后关闭 `stream`。函数一旦调用，这个`stream`会停止接受数据。

#### void dyad_close(dyad_Stream *stream)

立即关闭这个`stream`，丢弃写缓冲中的遗留数据。

#### void dyad_setTimeout(dyad_Stream *stream, double seconds)

设置`stream`在自动关闭之前，在没有经过任何活动的情况（接收中或发送中）下可以经过的秒数。如果设置为`0`秒，则`stream`从不超时，0是默认值。

#### void dyad_setNoDelay(dyad_Stream *stream, int opt)

如果 `opt` 是`1`，则禁用 `stream`中 `socket` 的 Nagle's 算法。`0` 是使能，默认是使能。

#### int dyad_getState(dyad_Stream *stream)

返回`stream`的当前状态，例如一个已经建立连接的流会返回 `DYAD_STREAM_CONNECTED`状态。令见 [States](#States)

#### const char* dyad_getAddress(dyad_Stream *stream)

返回`stream`的当前IP地址。用于监听 `stream` 则返回本机地址，已经建立连接的 stream 返回远程服务器地址。

#### int dyad_getBytesReceived(dyad_Stream *stream)

返回 `stream` 自当创建以来，接收到的字节数量。

#### int dyad_getBytesSent(dyad_Stream *stream)

返回 `stream`自当创建以来，发送的字节数量。它不包含 `stream` 中写缓冲，等待发送的数据。

#### dyad_Socket dyad_getSocket(dyad_Stream *stream)

返回 `stream`正在使用的socket。

### Event

#### DYAD_EVENT_DESTORY

当stream 销毁时发出。一旦此事件的所有监听器都返回，则关联流的指针不应该被视为有效。

#### DYAD_EVENT_ACCEPT

当监听`stream` 接收一个连接时发出。事件中 `remote`字段表示连接的客户端`stream`。

#### DYAD_EVENT_LISTEN

当监听 `stream`开始监听时发出。

#### DYAD_EVENT_CONNCET

当连接 `stream` 成功与其主机建立连接时发出。

#### DYAD_EVENT_CLOSE

当一个`steam`关闭时发送。关闭的流会自动销毁通过`dyad_update()`。另见 [DYAD_EVENT_DESTORY](#DYAD_EVENT_DESTORY)

#### DYAD_EVENT_READY

这个流开始准备发送大量数据时发送一次。这个事件通常用于当需要写一个很大的文件到一个stream，允许你发送较小的块，而不是一次拷贝所有数据到stream 的写buff中。

#### DYAD_EVENT_DATA

每当流接收数据时发出。这个事件的`data`数据段是接收到的数据（以NULL截止）。这个 `size`段是接收数据的大小（不包括用于截断的NULL）

#### DYAD_EVENT_LINE

每当接收一行数据时会发出。这个事件的`data`数据段是接收到的行，内容通过 `\n`、 `\r\n` 和 null 截止。`size` 字段表示字符串的长度。

#### DYAD_EVENT_ERROR

每当一个流发送错误时发出。例如，当创建一个监听socket而无法 bind 时发出这个事件。发出这个事件之后流会立即关闭。

#### DAYD_EVENT_TIMEOUT

当一个流超时时（see(`dyad_setTimeout()`)）. 发出这个事件后流会立即关闭。

#### DYAD_EVENT_TICK

每当一个tick 发生时发出。一个tick 是一个时间发出，以恒定间隔在每个流上。这个间隔可以通过使用 `dyad_setTickInterval()`设置。

## States

#### DYAD_STATE_CLOSED

这个steam 是关闭的。流在创建时是这个状态，如果保持此状态，则会自动销毁。

#### DYAD_STATE_CLOSING

这个流已经建立连接但写缓冲中还有数据，等待在关闭之前发送。

#### DYAD_STATE_CONNECTING

流在尝试连接到一个主机。

#### DYAD_STATE_CONNECTED

这个流已经建立连接，可以发送和接收数据

#### DYAD_STATE_LISTENING

这个流正在监听要接受的连接。

## Types

#### dyad_Socket

代表给定平台的一个socket。

#### dyad_Stream

通过 `dyad_newStream()` 创建的一个流或者监听服务器接收的一个连接。dyad 处理分配给所有流的资源。通过 `dyad_update()` 函数关闭流并自动销毁。

#### dyad_Event

当一个事件发生，一个包含事件信息的指针传递到 `dyad_Callback` 函数。这个结构体包含以下字段：

| 字段                | 描述                                             |
| ------------------- | ------------------------------------------------ |
| int type            | 发送事件的类型                                   |
| void *udata         | 这个 `udate`指针传递到 `dyad_addEventListener()` |
| dyad_Stream *stream | 发送这个事件的流                                 |
| dyad_Stream *remote | 接受连接时的客户端流                             |
| const char *msg     | 事件或错误信息的描述                             |
| char *data          | 事件的关联数据                                   |
| int size            | 事件关联数据的字节大小                           |

dyad_Event 结构体和 `data`字段指向的数据，不应该通过事件回调函数进行修改。

#### dyad_Callback

一个事件监听回调函数可以传递给 `dyad_addListener()`。这个函数应该是以下形式：

```c
void func(dyad_Event *event);
```

#### dyad_PanicCallback

一个atPanic 回调函数可以传递到 dyad_atPanic()。这个函数应该是以下形式：

```c
void func(const char *message);
```





<br>

<center>  ·End·  </center>
