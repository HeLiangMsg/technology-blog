---
title: "model-view-programming"
date: 2023-02-17T22:40:19+08:00
draft: false
tags: ["MVC"]
categories: ["Qt"]
featured_image: 
description: 
---

Qt 包含了一组item view 类，这些类使用模型/视图架构来管理数据之间的关系以及呈现给用户的方式。此体系结构引入的功能分离使开发人员能够更灵活地自定义项的表示方式并提供标准模型接口，以允许将各种数据源和现有  item view 一起使用。在本文档中，我们简要介绍了模型/视图范式，概述了所涉及的概念，并描述了 item view system的体系结构。解释了体系结构中的每个组件，并提供了如何使用所提供类的示例演示。

## 模型/视图体系结构

Model-View-Controller（MVC）是一种源自Smalltalk的设计模式，在构建用户界面时经常使用。在Design Patterns中，Gamma等人写到：

> MVC 有三种对象组成。Model 是应用程序对象，View 是其屏幕表示，控制器定义用户界面对用户输入的反应方式。在MVC 之前，用户界面设计倾向于将这些对象混为一谈。MVC 将他们解耦，以提高灵活性和重用性。

如果视图和控制器对象组合在一起，则结果是模型/视图体系结构。这仍然将数据存储的方式与呈现给用户的方式分开，但提供了基于相同原则的更简单的框架。这种分离使得可以在多个不同的视图中显示相同的数据，并实现新类型的视图，而无需更改基础数据结构。为了灵活处理用户输入，我们引入了**委托**的概念。**在此框架中委托具有的优点是，它允许自定义呈现和编辑数据项的方式。**

![img](https://doc.qt.io/qt-5/images/modelview-overview.png)

**模型/视图体系结构**

- 模型与数据源通信，为体系结构中的其他组件提供**访问接口**。通信的性质取决于数据源的类型，以及模型的实现方法。
- 视图从模型中获取**模型索引**；这些是对数据项的引用。通过向模型提供模型索引，视图可以从数据源检索数据项。
- 在标准视图中，**委托**呈现数据项。编辑项目时，委托使用模型索引直接与模型通信。

通常，模型/视图类可以分为上述三组：模型、视图和委托。这些组件中的每一个都由**抽象类定义**并提供通用接口，在某些情况下，还提供功能的默认实现。抽象类旨在被子类化，以便提供其他组件所期望的完整功能集，这样允许编写专门的组件。

模型、视图和委托使用信号槽进行相互通信：

- 来自模型的信号通知视图**有关数据源所持有数据的更改**
- 来自视图的信号提供有关**用户与所显示项的交互信息**
- 来自委托的信号在编辑期间用于告知模型和视图**有关编辑器的状态**

### 模型

所有item模型都基于 QAbstractModel 类。此类定义了视图和委托用于范围数据的接口。数据本身不必存储在模型中；它可以保存在由单独的类、文件、数据块或某些其它应用组件提供的数据结构或存储库中。

有关模型的基本概念在[模型类](https://doc.qt.io/qt-5/model-view-programming.html#model-classes)部分中介绍。

QAbstractItemModel 提供了一个足够灵活的数据接口，可以处理以表格、列表和数的形式表示数据的视图。但是，在为列表和类似表格的数据结构实现新模型时，QAbstractListModel 和 QAbstractTableModel 类是更好的起点，因为它们提供了常用函数的适当默认实现。这些类中的每一个都可以子类化，以提供支持专用类型的列表和表格的模型。

子类化模型的过程在[创建新模型](https://doc.qt.io/qt-5/model-view-programming.html#creating-new-models)一节中讨论。

Qt 提供了一些现成的模型，可用于处理数据项：

- QStringListModel 用于存储 QString 项的简单列表
- QStandItemModel 管理更复杂的item 数结构，每个item都可以包含任意数据
- QFileSystemModel 提供有关本地文件系统中的文件和目录的信息
- QSqlQueryModel、QSQLTableModel 和 QSqlRelationalTableModel 用于使用模型/视图约定访问数据库。

如果这些标准都不能满足你的需求，你可以子类化QAbstractItemModel ，QAbstractListModel或QAbstractTableModel来创建你自己的自定义模型。

### 视图

为不同类型的视图提供了完整的实现：QListView 显示向的列表，QTableView 在表格中显示模型的数据，QTreeView 在分层列表中显示数据的模型的项。这些类中的每一个都基于 QAbstractItemView抽象基类。经过这些类是即用型实现，但它们也可以进行子类化以提供自定义视图。

可用视图在[视图类](https://doc.qt.io/qt-5/model-view-programming.html#view-classes)部分中进行检查。

## 代理

QAbstractItemDelegate 是模型/视图框架中委托的抽象基类。默认委托实现由QStyleItemDelegate提供，Qt 的标准视图将其用作默认委托。However, [QStyledItemDelegate](https://doc.qt.io/qt-5/qstyleditemdelegate.html) and [QItemDelegate](https://doc.qt.io/qt-5/qitemdelegate.html) are independent alternatives to painting and providing editors for items in views. 它们之间的区别在于 QStyledItemDelegate 使用当前样式来绘制其项目。因此，我们建议在实现自定义委托或使用Qt样式表时 使用 QStyleItemDelegate作为基类。

委托在[委托类](https://doc.qt.io/qt-5/model-view-programming.html#delegate-classes)一节中描述。

### 排序

在模型/视图体系框架中有两种排序方法：选择哪种方法解决于你的基础模型。

如果你的模型是可排序的，即如果他重新实现了 QAbstractItemModel::sort() 函数，则QTableView 和 QTreeView 都提供了一个API，允许你编程方式对模型数据进行排序。此外，你还可以通过将 QHeaderView::sortIndicatorChanged() 信号分别连接到 QTableView::sortByColumn()槽函数或QTreeView::sortByColumn() 槽函数来启动交互式排序（即允许用户通过点击视图的标题对数据进行排序）。

如果模型没有所需的接口，或者要使用列表视图来显示数据，则替代方法是在视图中显示数据之前，使用a proxy model 去转换模型的结构。这在 [代理模型](https://doc.qt.io/qt-5/model-view-programming.html#proxy-models)一节中有详细介绍。

### 便利课程

许多便利类是从标准视图类派生的，for the benefit of applications that rely on Qt's item-based item view and table classes. 它们不打算被子类化。

此类的类实例包括：QListWidget，QTreeWidget和 QTableWidget。

这些类不如视图类灵活，不能与任意模型一起使用。我们建议你使用模型/视图方法来处理项目视图中的数据，除非你非常需要一组基于item的类。

如果你希望利用模型/视图方法提供的功能，同时仍使用基于item的界面，请考虑使用视图类，例如：QListView，QTableView和QTreeView 以及QStandardItemModel。

## 使用模型和视图

以下部分介绍了如何在Qt 中使用模型/视图模型。每个部分都包含一个实例，后面是一个显示如何创建新组建的部分。

Qt 提供的两种标准模型是QStandardItemModel 和 QFileSystemModel。QStandardItemModel是一个多用途的模型，可用于表示列表、表格和树视图所需的各种不同数据结构。此模型还保存数据项。QFileSystemModel 是维护有关目录内容的信息的模型。因此，它本身不保存任何数据项，而只是表示本地文件系统上的文件和目录。

QFileSystemModel 提供了一个现成的模型进行试验，并且可以轻松配置为使用现有的数据。使用此模型，我们可以展示如何设置用于现成视图的模型，并探索使用模型索引操作数据。

### 将视图与现有模型结合使用

QListView 和QTreeView 类是最适合于QFileSystemModel 一起使用的视图。下面显示的示例在列表视图中的相同的信息旁边显示树视图中目录的内容。视图共享用户的选择，以便在两个视图中突出显示所选项目。

![img](https://doc.qt.io/qt-5/images/shareddirmodel.png)

我们设置了一个QFIleSystemModel，以便它可供使用，并创建一些视图来显示目录的内容。这显示了使用模型最简单的方法。模型的创建和使用是在单个函数中执行的。

```cpp
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);


    QSplitter *splitter = new QSplitter; //The QSplitter class implements a splitter widget.
    QFileSystemModel * model = new QFileSystemModel;
    model->setRootPath(QDir::currentPath());
```

该模型设置为使用来自特定文件系统的数据。对 setRootPath 的调用告诉模型文件系统上哪个目录要向视图公开。

我们创建了两个视图，以便可以通过两种不同的方式检查模型中保存的项目。

```cpp
	QTreeView  *tree = new QTreeView(splitter);
    tree->setModel(model);
    tree->setRootIndex(model->index(QDir::currentPath()));//Sets the root item to the item at the given index.

    QListView *list = new QListView(splitter);
    list->setModel(model);
    list->setRootIndex(model->index(QDir::currentPath()));
```

视图的构建方式与其它widget相同。设置view以显示model中的item，只需使用目录模型作为参数调用其 setModel 函数即可。我们通过在每个视图上调用 setRootIndex 函数来过滤模型提供的数据，从文件系统模型中为当前目录传递合适的模型索引。

在这种情况下，使用的函数是QFileSystemModel  独有的。我们为其提供一个目录，它返回一个模型索引。Model indexes are discussed in [Model Classes](https://doc.qt.io/qt-5/model-view-programming.html#model-classes).

函数的其余部分仅显示了  QSplitter 的显示，并运行应用程序的时间循环。

```cpp
	splitter->setWindowTitle("Two views onto the same file system model");
    splitter->show();


    return app.exec();
```

在上面的例子中，我们忽略了如何处理item的选择。有关[处理项目视图中的选择](https://doc.qt.io/qt-5/model-view-programming.html#handling-selections-in-item-views)部分将更详细地介绍此主题。

## 模型类

在检查如何处理选择之前，你可能会发现检查模型/视图框架中使用的概念很有用。

### 基本概念

在模型/视图体系结构中，模型提供了一个标准接口，视图和委托使用该接口来访问数据。在Qt 中，标准接口由QAbstractItemModel 类定义。无论数据项如何存储在任何底层数据结构中，QAbstractItemModel 的所有子类都将数据表示为包含项表的分层结构。视图使用此约定 访问模型中的数据项，但它们向用户呈现此信息的方式不受限制。

![img](https://doc.qt.io/qt-5/images/modelview-models.png)

模型还通过信号和槽机制通知任何附加的视图有关数据更改的信息。

本节介绍了一些基本概念，这些概念对于其它组件通过模型类访问数据项的方式至关重要。后面的章节将讨论更高级的概念。

### 模型索引

为了确保数据的表现形式与访问数据的方式分开，引入了模型索引的概念。可以通过模型获得的每条信息都由模型索引表示。视图和委托使用这些索引来请求要显示的数据项。

因此，只有模型需要知道如何获取数据，并且可以相当笼统地定义模型管理的数据类型。模型索引包含指向创建它们的模型的指针，这可以防止在使用多个模型时出现混淆。

```cpp
QAbstractItemModel *model = index.model();
```

模型索引提供对信息片段的临时引用，并可用于通过模型检索或修改数据。由于模型可能会不时重组其内部结构，因此模型索引可能会变得无效，因此不应存储。如果需要一条信息进行长期引用，则必须创建 持久模型索引。这提供了对模型保持最新信息的引用。临时模型索引有 QModelIndex 类提供，持久型索引有 QPersistentModelIndex 类提供。

若要获取与数据项对应的模型索引，必须为模型指定三个属性：行号、列号和父项的模型索引。以下各节详细介绍和解释这些属性。

### 行和列

在其最基本的形式中，模型可以作为一个简单的表格进行访问，其中的item按其行号和列号定位。这并不意味这底层数据存储在数组结构中;行号和列号的使用只是允许组件相互通信的约定。我们可以通过向模型指定其行号和列号来检索有关任何给定item的信息，并且我们会收到一个表示该item的索引。

```cpp
QModelIndex index = model->index(row, column, ...);
```

为简单的单级数据结构（如列表和表格）提供接口的模型不需要提供任何其它信息，但是，如上面的代码所示，我们需要在获取模型索引时提供更多的信息。

![img](https://doc.qt.io/qt-5/images/modelview-tablemodel.png)

**行和列**

该图表示来了一个基本表格模型的表示方式，其中每个item都是由一对行号和列号定位。我们通过向模型传递相关的行号和列号来获取引用数据项的模型索引。

```cpp
QModelIndex indexA = model->index(0, 0, QModelIndex());
QModelIndex indexB = model->index(1, 1, QModelIndex());
QModelIndex indexC = model->index(2, 1, QModelIndex());
```

始终通过指定QModelIndex()为模型中的父项来引用模型中的顶级项。这将在下一节中讨论。
### 项目的父项

在表格或列表视图中使用数据时，模型提供的项数据的，类似表格的节目是理想的选择；行号和列号系统与视图显示项目的方式完全一致。的那是，树视图等结构要求模型向其中的项公开更灵活的接口。因此，每个项目也可以是另一个表格项的父项，其方式与树视图中的顶级项可以包含另一个列表项的方式大致相同。

请求模型项的索引时，我们必须提供有关于父项的一些信息。在模型之外，引用项目的唯一方法是通过模型索引，因此还必须给出父模型索引。

```cpp
QModelIndex index = model->index(row, column, parent);
```

![img](https://doc.qt.io/qt-5/images/modelview-treemodel.png)

**父项、行和列**

该图显示了树模型的表示形式，其中每个项目都是由父项、行号和列号进行引用。

项A 和C 在模型中表示为顶级项的同级。

```cpp
QModelIndex indexA = model->index(0, 0, QModelIndex());
QModelIndex indexC = model->index(2, 1, QModelIndex());
```

项目A 有许多子项。使用一下代码来获取A下面的子项B的模型索引。

```cpp
QModelIndex indexB = model->index(1, 0, indexA);
```

**项目角色**

模型中的项可以为相关联组件提供各种角色，从而允许为不同情况下提供不同类型的数据。例如：Qt::DispalyRole 用于访问可在视图中显示为文本的字符串。通常，item包含许多不同角色的数据，标准角色由Qt::ItemDataRole 定义。

我们可以通过向模型传递与项对应的模型索引，并通过指定一个角色来获取我们想要的数据类型，从而向模型询问项的数据。

```cpp
QVariant value = model->data(index, role);
```

![img](https://doc.qt.io/qt-5/images/modelview-roles.png)

**项目角色**

该角色向模型表示引用的数据类型。视图可以以不同的方式显示角色，因此为每个角色提供适当的信息非常重要。[创建新模型](https://doc.qt.io/qt-5/model-view-programming.html#creating-new-models)部分更详细地介绍了角色的一些特定用法。

### 总结

- 模型索引以独立于任何基础数据结构的方式，提供有关模型的项位置和委托信息
- 项是有其行号和列号以及其父项的模型索引引用。*其中在表格项的索引中，父项一直为顶级项*
- 如果使用index() 请求索引时，为父项指定了有效的模型索引，则返回的索引将引用模型中的该父项下的项。获取的索引对应的是该项的子项引用。
- 如果使用 index() 请求索引时为父项指定了无效的模型索引，则返回的索引将引用模型中的顶级项。
- 角色区分与项关联的不同类型的数据

<br>

<center>  ·End·  </center>
