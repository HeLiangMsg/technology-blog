---
title: "获取时间"
date: 2022-05-28T23:07:26+08:00
draft: false
tags: []
categories: ["源码"]
featured_image: 
description: 
---
```c
 #include <sys/time.h>
 #include <stdio.h>

 double dyad_getTime(void)
 {
     struct timeval tv;
     gettimeofday(&tv, 0);

     printf("%lu \t %lu \n", tv.tv_sec, tv.tv_usec);                                                                                                                  
     return tv.tv_sec + tv.tv_usec / 1e6;
 }
 int main()
 {
     printf("%f \n", dyad_getTime());

    return 0;
 }
```

输出内容为：

> 1655386427       123751
> 1655386427.123751



`gettimeofday` 返回当前的时间，用来计算动作的耗时。`tv_tv_sec` 是秒数， `tv_tv_usec` 是微妙



<br>

<center>  ·End·  </center>
