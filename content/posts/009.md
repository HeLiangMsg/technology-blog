---
title: "CMake Tutorial"
date: 2022-04-18T23:00:33+08:00
draft: false
tags: ["cmake"]
categories: ["工具"]
featured_image: 
description: 
---
[TOC]

# CMake Tutorial

## 一个基础的起点_1

最基本的项目是从源代码文件构建的可执行文件。对于简单的项目，只需要三行CMakeLists.txt文件。这将是本教程的起点。在Step1目录中创建一个CMakeLists.txt文件，如下所示：

```cmake
cmake_minimum_required(VERSION 3.10)

# set the project name
project(Tutorial)

# add the executable
add_executable(Tutorial tutorial.cxx)
```

请注意，此示例在CMakeLists.txt文件中使用小写命令。 CMake支持大写，小写和大小写混合命令。 step1目录中提供了`tutorial.cxx`的源代码，可以打印一个 “Hello，CMake！”。

### 添加一个版本号和配置头文件

我们将添加的第一个功能是为我们的可执行文件和项目提供版本号。尽管我们可以仅在源代码中执行此操作，但是使用`CMakeLists.txt`可以提供更大的灵活性。

首先，修改CMakeLists.txt文件以使用`project()`命令设置项目名称和版本号。

```cmake
cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(Tutorial VERSION 1.0)
```

然后，配置一个头文件以将版本号传递给源代码：

```cmake
configure_file(TutorialConfig.h.in TutorialConfig.h)
```

由于已配置的文件将被写入二进制树，因此我们必须将该目录添加到路径列表中以搜索包含文件。将以下行添加到CMakeLists.txt文件的末尾：

```cmake
target_include_directories(Tutorial PUBLIC
                           "${PROJECT_BINARY_DIR}"
                           )
```

补充：如果不写，貌似也没有事。

使用你喜欢的编辑器，在源目录中使用以下内容创建`TutorialConfig.h.in`：

```cpp
// the configured options and settings for Tutorial
#define Tutorial_VERSION_MAJOR @Tutorial_VERSION_MAJOR@
#define Tutorial_VERSION_MINOR @Tutorial_VERSION_MINOR@
```

当CMake配置此头文件时，`@ Tutorial_VERSION_MAJOR @`和`@ Tutorial_VERSION_MINOR @`的值将被替换。会生成一个`TutorialConfig.h`

接下来，修改`tutorial.cxx`以包括配置的头文件`TutorialConfig.h`。

最后，通过更新`tutorial.cxx`包含以下内容，来打印出可执行文件的名称和版本号：

```cpp
if (argc < 2) 
{
    // report version
    std::cout << argv[0] << " Version " << Tutorial_VERSION_MAJOR << "."
              << Tutorial_VERSION_MINOR << std::endl;
    std::cout << "Usage: " << argv[0] << " number" << std::endl;
    return 1;
}
```

### 指定C++ 标准

接下来，向我们的项目中添加一些C ++ 11功能。

```cpp
auto Value = 5.5;
```

我们将需要在CMake代码中明确声明应使用正确的标志。

在CMake中启用对特定C ++标准的支持的最简单方法是使用`CMAKE_CXX_STANDARD`变量。对于本教程，将CMakeLists.txt文件中的`CMAKE_CXX_STANDARD`变量设置为11，并将`CMAKE_CXX_STANDARD_REQUIRED`设置为True。确保在对add_executable的调用上方添加`CMAKE_CXX_STANDARD`声明。

```cmake
cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(Tutorial VERSION 1.0)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)
```

### 编译并测试

运行cmake可执行文件或cmake-gui来配置项目，然后使用所选的构建工具对其进行构建。

例如在当前目录中创建一个编译目录

```bash
$mkdir Step1_build
```

接下来，导航到构建目录并运行CMake来配置项目并生成本机构建系统：

```bash
$cd Step1_build
$cmake ../
```

然后调用该构建系统以实际编译/链接项目：

```bash
$ cmake --build .
```

最后，尝试使用新构建的`Tutorial`：

```bash
step1/Step1_build$ ./Tutorial 
./Tutorial Version 2.2
Usage: ./Tutorial number
6
step1/Step1_build$ 
```

## 添加一个库_2

现在，我们将库添加到我们的项目中。该库将包含我们自己的实现，用于计算数字的平方根。然后可执行文件可以使用此库，而不是使用编译器提供的标准平方根函数。

在本教程中，我们将库放入名为`MathFunctions`的子目录中。该目录已经包含头文件`MathFunctions.h`和源文件`mysqrt.cxx`。源文件具有一个称为mysqrt的函数，该函数提供与编译器的sqrt函数类似的功能。

将以下一行`CMakeLists.txt`文件添加到MathFunctions目录中。（在MathFunctions目录下创建`CMakeLists.txt`）

```cmake
add_library(MathFunctions mysqrt.cxx)
```

为了利用新库，我们将在顶层`CMakeLists.txt`文件中添加一个`add_subdirectory()`调用，以便构建该库。我们将新库添加到可执行文件，并将MathFunctions添加为包含目录，以便可以找到`mysqrt.h`头文件。顶级`CMakeLists.txt`文件的最后几行现在应如下所示：

````cmake
add_subdirectory(MathFunctions)# 添加子目录
# add the executable
add_executable(Tutorial  tutorial.cxx)

target_link_libraries(Tutorial PUBLIC ${EXTRA_LIBS})# 添加连接库到 可执行文件

# 目标包含目录
target_include_directories(Tutorial PUBLIC
                           "${PROJECT_BINARY_DIR}"
                           ${EXTRA_INCLUDES}
                           )

# 使用变量EXTRA_LIBS来收集所有可选库，以供以后链接到可执行文件中。
# 变量EXTRA_INCLUDES类似地用于可选的头文件。
````

现在让我们将MathFunctions库设为可选。虽然对于本教程来说确实不需要这样做，但是对于大型项目来说，这是很常见的。第一步是向顶级`CMakeLists.txt`文件添加一个选项。

```cmake
option(USE_MYMATH "Use tutorial provided math implementation" ON)

# configure a header file to pass some of the CMake settings
# to the source code
configure_file(TutorialConfig.h.in TutorialConfig.h)
```

此选项将显示在`cmake-gui`和`ccmake`[^1]中，默认值ON可由用户更改。

[^1]:  An ncurses (terminal) GUI

此设置将存储在缓存中，因此用户无需在每次在构建目录上运行CMake时都设置该值。

下一个更改是使条件构建和链接MathFunctions库成为条件。为此，我们将顶级`CMakeLists.txt`文件的结尾更改为如下所示：

```cmake
if(USE_MYMATH)
  add_subdirectory(MathFunctions)# 添加子目录
  list(APPEND EXTRA_LIBS MathFunctions) #添加扩展库 MathFunctions 
  list(APPEND EXTRA_INCLUDES "${PROJECT_SOURCE_DIR}/MathFunctions") # 添加扩展源文件
endif()

# add the executable
add_executable(Tutorial  tutorial.cxx)
```

请注意，使用变量`EXTRA_LIBS`来收集所有可选库，以供以后链接到可执行文件中。变量`EXTRA_INCLUDES`类似地用于可选的头文件。当处理许多可选组件时，这是一种经典方法，我们将在下一步中介绍现代方法。

对源代码的相应更改非常简单。首先，如果需要，请在`tutorial.cxx`中包含`MathFunctions.h`标头：

```cpp
#ifdef USE_MYMATH
#  include "MathFunctions.h"
#endif
```

然后，在同一文件中，使`USE_MYMATH`控制使用哪个平方根函数：

```cpp
#ifdef USE_MYMATH
        const double outputValue = InvSqrt(inputValue);
#else
        const double outputValue = sqrt(inputValue);
#endif
```

由于源代码现在需要`USE_MYMATH`，因此我们可以使用以下行将其添加到`TutorialConfig.h.in`中：

````cmake
#cmakedefine USE_MYMATH
````

( 在`TutorialConfig.h` 会被转换为`#define USE_MYMATH  `。)

运行cmake可执行文件或`cmake-gui`来配置项目，然后使用所选的构建工具对其进行构建。然后运行构建的Tutorial可执行文件。

现在，让我们更新`USE_MYMATH`的值。如果您在终端中，最简单的方法是使用`cmake-gui`或ccmake。或者，如果您想从命令行更改选项，尝试：

```bash
cmake buidlDir -DUSE_MYMATH=OFF
```

重新编译并再次运行  Tutorial。输出的结果是什么？

### configure_file 用法

将文件复制到另一个位置并修改其内容。

```cmake
configure_file(<input> <output>
               [COPYONLY] [ESCAPE_QUOTES] [@ONLY]
               [NO_SOURCE_PERMISSIONS]
               [NEWLINE_STYLE [UNIX|DOS|WIN32|LF|CRLF] ])
```

将<input>文件复制到<output>文件，并在输入文件内容中替换引用为@VAR@或${VAR}的变量值。每个变量引用将替换为该变量的当前值，如果未定义该变量，则为空字符串。

此外，输入的形式为：

```cpp
#cmakedefine VAR ...
```

将被替换为

```cpp
#define VAR ...
```

或

```cpp
/* #undef VAR */
```

#### Example

创建一个源码树包含`foo.h.in`文件。

```cmake
#cmakedefine FOO_ENABLE
#cmakedefine FOO_STRING "@FOO_STRING@"
```

在`CMakeLists.txt` 中使用`configure_file` 来配置`foo.h`。

```cmake
option(FOO_ENABLE "Enable Foo" ON)
if(FOO_ENABLE)
  set(FOO_STRING "foo")
endif()
configure_file(foo.h.in foo.h @ONLY)
```

这将在与该源目录相对应的构建目录中创建`foo.h`。如果`FOO_ENABLE`选项为`ON`，则`configure.h`将包含：

```CPP
#define FOO_ENABLE
#define FOO_STRING "foo"
```

反则包含

```cpp
/* #undef FOO_ENABLE */
/* #undef FOO_STRING */
```

（也可以推导出，如果你**只在** `configure.h.in` 中`#cmakedefine VAR`， 而没有在`CMakeLists.txt`中 `set VAR`  ，

则构建后的 `configure.h` 中 只有 `/* #undef VAR */`，而不是 `#define VAR`）

然后可以使用`include_directories()`命令将输出目录指定为包含目录：

```cmake
include_directories(${CMAKE_CURRENT_BINARY_DIR})
```

这样就可以在源码中包含头文件`foo.h`。



## 添加库的使用要求_3

使用要求 可以更好地控制库或可执行文件的链接并包含行，同时还可以更好地控制CMake内部目标的传递属性。利用使用需求的主要命令是：

- target_compile_definitions()
- target_compile_options()
- target_include_directories()
- target_link_libraries()

让我们从添加库（第2步）中重构代码，以使用现代CMake的使用需求方法。我们首先声明，链接到MathFunctions的任何人都需要包括当前源目录，而MathFunctions本身不需要。因此，这可以成为接口使用要求。

请记住，`INTERFACE`表示消费者需要的东西，而生产者则不需要。将以下行添加到`MathFunctions / CMakeLists.txt`的末尾：

```cmake
target_include_directories(MathFunctions
          INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
          )
```

现在，我们已经指定了MathFunction的使用要求，现在可以在这里安全地从顶级`CMakeLists.txt`中删除对`EXTRA_INCLUDES`变量的使用

```cmake
if(USE_MYMATH)
  add_subdirectory(MathFunctions)
  list(APPEND EXTRA_LIBS MathFunctions)
endif()
```

和这里

```cpp
target_include_directories(Tutorial PUBLIC
                           "${PROJECT_BINARY_DIR}"
                           )
```

### 添加库的使用要求2



## 安装并测试_4

现在，我们可以开始向项目添加安装规则和测试支持。

### 安装规则

安装规则非常简单：对于MathFunctions，我们要安装库和头文件，对于应用程序，我们要安装可执行文件和配置的头文件。

因此，在MathFunctions / CMakeLists.txt的末尾，我们添加：

```cmake
install(TARGETS MathFunctions DESTINATION lib) #安装目标文件 到 指定目录的lib中
install(FILES MathFunctions.h DESTINATION include) #安装文件 到 指定目录的 include 中
```

这就是创建本教程的基本本地安装所需的全部。

现在，运行cmake可执行文件或`cmake-gui`来配置项目，然后使用所选的构建工具对其进行构建。

然后，通过命令行使用cmake命令的安装选项（在3.15[^2]中引入，较早版本的CMake必须使用`make install`）运行安装步骤。对于多配置工具，请不要忘记使用`--config`参数来指定配置。如果使用IDE，只需构建INSTALL目标。此步骤将安装适当的头文件，库和可执行文件。例如：

[^2]: Ubuntu 18.04.5 LTS中最新的cmake 版本为 3.10 ，就只能使用 make install 

```cmake
cmake --install .
```

CMake变量`CMAKE_INSTALL_PREFIX`用于确定文件的安装根目录。如果使用`cmake --install`命令，则可以通过`--prefix`参数覆盖安装前缀。例如：

```cmake
cmake --install . --prefix "/home/myuser/installdir"
```

导航到安装目录，并验证已安装的教程是否正在运行。

（不指定 `--prefix`，

- 在Windows 中会默认安装路径为` C:/Program Files (x86)/Tutorial`和` C:/Program Files (x86)/Tutorial/lib`下
- Linux 默认路径为`/usr/local/lib/` 和 `/usr/local/bin/`）

### 测试支持

接下来，测试我们的应用程序。

在顶级CMakeLists.txt文件的末尾，我们可以启用测试，然后添加一些基本测试以验证应用程序是否正常运行。

```cmake
enable_testing()

# does the application run
add_test(NAME test1 COMMAND Tutorial 25) 
# 函数原型 add_test(NAME <name> COMMAND <command> [<arg>...])

# does the usage message work?
add_test(NAME test2 COMMAND Tutorial)
set_tests_properties(test2
  PROPERTIES PASS_REGULAR_EXPRESSION "Usage:.*number"
)
# 函数原型：set_tests_properties(test1 [test2...] PROPERTIES prop1 value1 prop2 value2)
# PASS_REGULAR_EXPRESSION 是用于测试的属性之一 ： 程序的输出必须匹配正则表达式，测试才能通过 



# define a function to simplify adding tests
function(do_test target arg result) #函数名 测试程序名 参数 结果
  add_test(NAME Comp${arg} COMMAND ${target} ${arg})
  set_tests_properties(Comp${arg}
    PROPERTIES PASS_REGULAR_EXPRESSION ${result} 
  )
endfunction(do_test)

# do a bunch of result based tests
do_test(Tutorial 4 "2")
do_test(Tutorial 9 "3")
do_test(Tutorial 5 "2.236")
do_test(Tutorial 7 "2.645")
do_test(Tutorial 25 "5")
do_test(Tutorial -25 "[-nan|nan|0]")
do_test(Tutorial 0.0001 "0.01")
```



第一个测试只是验证应用程序正在运行，没有段错误或其他崩溃，并且返回值为零。这是CTest测试的基本形式。

下一个测试使用`PASS_REGULAR_EXPRESSION`测试属性来验证测试的输出是否包含某些字符串。在这种情况下，请验证在提供了错误数量的参数时是否打印了使用情况消息。

最后，我们有一个名为`do_test`的函数，该函数运行应用程序并验证所计算的平方根对于给定输入是否正确。对于`do_test`的每次调用，都会根据传递的参数将另一个测试（带有名称，输入和预期结果）添加到项目中。

重建应用程序，然后cd到二进制目录并运行ctest可执行文件：`ctest -N`和`ctest -VV`。对于多配置生成器（例如`Visual Studio`），必须指定配置类型。例如，要以Debug模式运行测试，请从构建目录（而不是Debug子目录！）中使用`ctest -C Debug -VV`。或者，从IDE构建`RUN_TESTS`目标。

>  enable_testing()  #对当前目录及以下版本启用测试。 该命令应位于源目录的根目录中，因为ctest希望在构建目录的根目录中找到一个测试文件。
>
> add_test()  \#将测试添加到要由ctest 运行的项目中，相当于创建了一个测试用例，然后ctest 会去调用它

## 安装发布_5

接下来，假设我们想将项目分发给其他人，以便他们可以使用它。我们希望在各种平台上提供二进制和源代码分发。这与我们之前在“安装和测试”（第4步）中进行的安装有些不同，在安装和测试中，我们正在安装根据源代码构建的二进制文件。在此示例中，我们将构建支持二进制安装和程序包管理功能的安装程序包。为此，我们将使用CPack创建特定于平台的安装程序。具体来说，我们需要在顶级`CMakeLists.txt`文件的底部添加几行。

```cmake
include(InstallRequiredSystemLibraries)
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/License.txt")
set(CPACK_PACKAGE_VERSION_MAJOR "${Tutorial_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${Tutorial_VERSION_MINOR}")
include(CPack)
```

这就是全部。我们首先包括`InstallRequiredSystemLibraries`。该模块将包括项目当前平台所需的任何运行时库。接下来，我们将一些CPack变量设置为存储该项目的许可证和版本信息的位置。版本信息是在本教程的前面设置的，并且`license.txt`*(自己创建)*已包含在此步骤的顶级源目录中。最后，我们包括CPack模块，该模块将使用这些变量和当前系统的其他一些属性来设置安装程序。

下一步是以通常的方式构建项目，然后运行cpack可执行文件。要构建二进制发行版，请从二进制目录运行：

```
cpack
```

使用cmake  需要在编译目录下执行。否则提示`CPack Error: CPack generator not specified`，执行cpack 的结果。

![cpack 输出](/media/009/image-20210411152013507.png)

要指定生成器，请使用`-G`选项。对于多配置构建，请使用`-C`指定配置。例如：

```bash
cpack -G ZIP -C Debug
```

调用脚本执行的结果

![install](/media/009/image-20210411152118414.png)




<br>

<center>  ·End·  </center>
